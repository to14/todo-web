

function orderLists() {
    let lista = document.getElementById('list')
    Sortable.create(lista, {
        animation: 150,
        chosenClass: "seleccionado",
        dragClass: "drag",
        onEnd: () => {

        },
        group: "list",
        store: {
            // guardar orden lista
            set: (sortable) => {
                const ordre  = sortable.toArray();
                // darle un data-id="" al elemento <ul/> en index.html

                // join coje cada elemnto del array y lo pasa a cadena de texto para poder guardarlo en localstorage
                localStorage.setItem(sortable.options.group.name, ordre.join(','));
            },

            // Obtenemos el orden de la lista del localStorage
            get: (sortable) => {
                const orden = localStorage.getItem(sortable.options.group.name);
                // Deja guardado el orden, compruebo si existe el orden vuelvo a convertir laca cadena de texto en array para guarlo y
                // cada vez que recargue la paagina se gurade el orden al hacer drag and drop, si el orden no existe lo que hago es devolver un array vacio
                return orden ? orden.split(',') : [];

            }
        }
    })
}
