var api = new class {

    user = null
    apiURL = "https://tdboard.herokuapp.com"
    // "http://localhost:8080/"
    listas = null
    tasques = null

    colors = ["#ffffcc", "#ffccff", "#ccffff", "#ccccff", "#ccffcc", "#ffcccc"]

    constructor () {
        if (!localStorage.userid)
            openFormNewUser()
        else
            this.getUserById(localStorage.userid,function(usuario){
                api.setUser(usuario)
                orderLists()
            })
    }

    setUser(user) {
        if (user.listas == null) user.listas = []
        this.user = user
        for (let lista of user.listas) {
            addList(lista.title, lista.id, lista.color)
        }
    }

    send (method, endpoint, data, onload) {
        var req = new XMLHttpRequest()
        data = data ? data : ""
        req.onload = (e) => {
            if (onload)
                onload(JSON.parse(req.response))
        }
        req.open(method, this.apiURL+endpoint)
        req.setRequestHeader("Content-Type", "application/json")
        req.send(JSON.stringify(data))
    }

    registerUser (action) {
        this.send("POST", "/todousers", {}, action)
    }

    getTasksByIdList (id) {
        for (let tasques of this.user.listas){
            if (id == tasques.id){
                return tasques.tasks
            }
        }    
    }

    getTaskById (id) {
        for (let list of this.user.listas){
            for (let tasca of list.tasks) {
                if (tasca.id == id) return tasca
            }
        }
        return null
    }

    getLists () {
        return this.user.listas
    }

    getListById (id) {
        for (let dades of this.user.listas){
            if(id == dades.id){
                return dades
            }
        }
    }

    addList (title, action) {
        var data = {
            title: title,
            color: this.colors[Math.floor(Math.random()*this.colors.length)],
            users: [this.user]
        }

        this.send("POST", "/todolists", data, action)
        return data
    }


    updateUser () {
        this.send("PUT", "/todousers", this.user, null)
    }
    
    addTask (name, desc, action) {
        var tasca = {
            name: name,
            description: desc,
            status: true
        }

        this.send("POST", "/todoitems", tasca, action)
        return tasca
    }

    removeList (id) {
        console.log(this.user)
        for (let lista of this.user.listas) if (lista.id == id)
            this.user.listas.splice(this.user.listas.indexOf(lista), 1)
        console.log(this.user)

        this.updateUser()
        this.send("DELETE", "/todolists/"+id)
    }

    getUserById(id, action){
        this.send("GET", "/todousers/"+id, null,action)
    }

    modifyList (id, title, color, action) {
        var llista = this.getListById(id)
        llista.title = title
        llista.color = color
        for (var i = this.user.listas.length - 1; i >= 0; i--) {
            if (this.user.listas[i].id == id) {
                this.user.listas[i] = llista
                break
            }
        }
        this.send("PUT", "/todolists", llista, action)
    }

    removeTask (id) {
        // -_- 
        //api.send("DELETE", "/todoitems/"+id, null, (e) => {console.log(e)})
        for (var i = this.user.listas.length - 1; i >= 0; --i) {
            for (var j = 0; j < this.user.listas[i].tasks.length; j++) {
                if (this.user.listas[i].tasks[j].id == id) {
                    this.user.listas[i].tasks.splice(j, 1)
                    this.send("PUT", "/todolists", this.user.listas[i])
                    return
                }
            }
        }
    }

    addTaskToList (listId, task, action) {
        let lista = this.getListById(listId)
        lista.tasks.push(task)
        this.send("PUT", "/todolists", lista, action)
    }
    
    modifyTask (task, action){
        this.send("PUT", "/todoitems", task, action)
        for (var i = this.user.listas.length - 1; i >= 0; i--) {
            let lista = this.user.listas[i]
            for (var j = lista.tasks.length - 1; j >= 0; j--) {
                let tasca = lista.tasks[j]
                if (tasca.id == task.id) {
                    lista.tasks[j] = task
                    this.user.listas[i] = lista
                    return
                }
            }
        }
    }

    registerCode (code, action) {
        this.send("GET", "/todolists/"+code, null, function(lista){
            api.user.listas.push(lista)
            api.updateUser()
            if(action)action(lista)
        })
    }

}
