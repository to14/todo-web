
var fadeout = null


window.onkeydown = function (event){
    
    if (event.key == 'Enter' && event.target == document.body){
        openFormNewList()
    }
    else if (event.key == 'Escape'){
        close('tasks')
    }else {
        //console.log(event.target.tagName)
        let form = document.getElementById('form')
        if (form && event.target.tagName != 'INPUT')
        {
            
            form.querySelector('form').elements[0].focus()
        
        } else {


            if (
                document.body.offsetWidth < 650 ||
                event.target.tagName != "INPUT" ||
                event.target.id == "buscar-pal") return


            let bounds = event.target.getBoundingClientRect()

            let hand = document.getElementById("hand")

            if (fadeout) clearInterval(fadeout)
            hand.style.opacity = 1
            fadeout = setInterval(function(){hand.style.opacity = 0}, 1000)



            let deg = Math.random()*10 - 5
            hand.style.transform = `rotate(${deg}deg)`

            hand.style.animation = "none"
            hand.style.animation = "animation: 1s fadeout forwards"
            hand.style.top = `calc(${bounds.top}px + ${bounds.height}px / 2 - 110px)`
            hand.style.left = `calc(${bounds.left}px + ${event.target.selectionStart}em * .9)`
            hand.style.display = "block"

        }

    }

    
}

window.onmousedown = function(event) {
	let m = document.getElementById("menu")
	if (event.target != m && event.target.parentElement != m) {
		close("menu")
	}
}
