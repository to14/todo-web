
const levels = ["lists", "tasks", "form", "menu"]


// Cierra el nivel indicado y los niveles inferiores
function close(level) {
	let l = [...levels]
	let toClose = l.splice(levels.indexOf(level))
	for (let c of toClose) {
		c = document.getElementById(c)
		if (c) c.remove()
	}
}

function menu(event, type) {
	event.preventDefault()
	let m = document.createElement("div")
	m.id = "menu"
	m.style.top = event.pageY+"px"
	m.style.left = event.pageX+"px"
	m.style.display = "block"
	document.body.appendChild(m)

	let target = event.target

	let code = target.getAttribute("code")

	while (!code)  {
		target = target.parentElement
		code = target.getAttribute("code")
	}
	switch (type) {
		case "list":
			m.innerHTML = `
				<button onclick='modifyList(${code})'>Modificar</button>
				<button onclick='removeList(${code})'>Eliminar</button>
				<button onclick='showCode(${code})'>Compartir</button>
			`
			break
		case "task":
			m.innerHTML = `
				<button onclick='modifyTask(${code})'>Modificar</button>
				<button onclick='removeTask(${code})'>Eliminar</button>
			`
	}
	return false
}


var activeTaskCode = 0
function openTasks(code) {
	close("tasks")
	if (activeTaskCode == code) return (activeTaskCode=0)
	
	let list = api.getListById(code)
	let tasks = list.tasks
	let section = document.createElement("section")
	section.id = "tasks"
	section.setAttribute("data-id", code)
	let tag = document.createElement("h2")
	tag.innerHTML = list.title
	tag.id = "tag"
	let shareCode = document.createElement("h4")
	shareCode.innerHTML = list.id
	shareCode.id = "shareCode"
	section.appendChild(shareCode)
	tag.style.backgroundColor = list.color
	shareCode.style.color = list.color
	section.appendChild(tag)
	let container = document.createElement("div")
	section.appendChild(container)
	if (tasks) for (let task of tasks) {
		let a = document.createElement("a")
		a.classList.add("task")
		a.innerHTML = `
			<h3>${task.name}</h3>
			<p>${task.description}</p>
		`
		a.setAttribute("code", task.id)
		a.onclick = function() {
			var t = api.getTaskById(task.id)
			t.status = !t.status
			//var t = {id : task.id, name : task.name, description: task.description, status: !task.status}
			api.modifyTask(
				t, 
				(r) => {
					if(t.status){
						this.classList.remove("disabled")
					} else {
						this.classList.add("disabled")
					}
				}
			)
		}
		a.oncontextmenu = function(event) {
			menu(event, "task")
		}

		if (!task.status)
			a.classList.add("disabled")
		container.appendChild(a)
	}
	Sortable.create(container, {
		animation: 150,
		chosenClass: "seleccionado",
		dragClass: "drag",
		group: "tasks-"+code,
		store: {
            // guardar orden lista
            set: (sortable) => {
                const ordre  = sortable.toArray();
                // darle un data-id="" al elemento <ul/> en index.html

                // join coje cada elemnto del array y lo pasa a cadena de texto para poder guardarlo en localstorage
                localStorage.setItem(sortable.options.group.name, ordre.join(','));
            },

            // Obtenemos el orden de la lista del localStorage
            get: (sortable) => {
                const orden = localStorage.getItem(sortable.options.group.name);
                // Deja guardado el orden, compruebo si existe el orden vuelvo a convertir laca cadena de texto en array para guarlo y
                // cada vez que recargue la paagina se gurade el orden al hacer drag and drop, si el orden no existe lo que hago es devolver un array vacio
                return orden ? orden.split(',') : [];

            }
        }
	})

	let newTaskButton = document.createElement("button")
	newTaskButton.innerHTML = '<i class="fa fa-plus"></i>'
	newTaskButton.onclick = function (){
		openFormNewTask(code);
	}
	newTaskButton.className = "bNewTask"
	section.appendChild(newTaskButton)
	newTaskButton.style.backgroundColor = list.color
	document.body.appendChild(section)
	activeTaskCode = code
}
function form(event, type) {
	event.preventDefault()
	let m = document.createElement("div")
	m.id = "form"
	m.style.top = event.pageY+"px"
	m.style.left = event.pageX+"px"
	m.style.display = "block"
	document.body.appendChild(m)
	switch (type) {
		case "tasks":
			let code = event.target.getAttribute("code")
			m.innerHTML = `
				<button onclick='modifyList(${code})'>Modificar</button>
				<button onclick='removeList(${code})'>Eliminar</button>
				<button onclick='showCode(${code})'>Compartir</button>
			`
			break
	}
	return false
}

function openSettings(){
	let section = document.createElement("section")
	section.id = "form"

	let formSettings = document.createElement("form")
	section.appendChild(formSettings)

	formSettings.onsubmit = function(event){
		
		event.preventDefault()

		let newCode = event.target.elements.user.value


		localStorage.userid = newCode
		window.location = window.location

	}

	formSettings.innerHTML = `
		<input name='user' placeholder='insereix el teu ID' type='text' value='${api.user.id}'>
		<input type='submit' value='Importar usuario'>`
	
	document.body.appendChild(section)	
}

function openFormNewUser(){
	
	let section = document.createElement("section")
	section.id = "form"

	let formUser = document.createElement("form")
	section.appendChild(formUser)

	formUser.onsubmit = function(event){

		event.preventDefault()

		let codeUser = event.target.elements.code.value

		if (!codeUser){
			api.registerUser(function(usuario){
                api.setUser(usuario)
				localStorage.userid = usuario.id
            })
		}
		else{
			api.getUserById(codeUser,function(usuario){
                api.setUser(usuario)
				localStorage.userid = usuario.id
            })
		}
		close("form")
	}

	formUser.innerHTML = `
		<input name='code' placeholder='Código de Usuario (opcional)' type='text'>
		<input type='submit' value='Iniciar sesión'>`

	document.body.appendChild(section)	
}

function openFormNewList() {
	close("tasks")
	activeTaskCode = 0
	let section = document.createElement("section")
	section.id = "form"

	let form = document.createElement("form")
	section.appendChild(form)
	form.onsubmit = function(event) {
		event.preventDefault()

		let code = event.target.elements.code.value
		let name = event.target.elements.name.value
		
		if (!name && !code) return close("form")

		if (code) api.registerCode(code, function(lista){
			addList(lista.title, lista.id, lista.color)
		})

		let list = code ? api.getListById(code) : api.addList(name, (r) => {
			if (r) {
				r.tasks = []
				api.user.listas.push(r)
				api.updateUser()
				addList(r.title, r.id, r.color)
			}
		})
		close("form")

		return false
	}

	form.innerHTML = `
		<input id='focus' type='text' name='name' placeholder='Nombre'>
		o
		<input type='text' name='code' placeholder='Código Lista Compartida'>
		<input type='submit' value='Crear'>
		<button type='button' onclick="window.close('form')">Cancelar</button>
	`

	document.body.appendChild(section)
}

window.onload = function (){
	document.getElementById("buscar-pal").onkeyup = function(){
		let inputSearch = document.getElementById("buscar-pal")
		let box_Search = document.getElementById("list")
		filter = inputSearch.value.toUpperCase()
		let li = box_Search.getElementsByTagName("li")
		for (let l of li){
		}

		for ( let l of li){
			let a = l.getElementsByTagName("a")[0]
			textValue = a.textContent || a.innerHTML

			if (textValue.toUpperCase().indexOf(filter) > -1){
				l.style.display = "";
				box_Search.style = "block"

				if (inputSearch.value === ""){
					box_Search.style.display = "nome"
				}


			}else{
				l.style.display = "none";
			}
		}

	}
}

function openFormModifyList(lista) {

	close("tasks")
	activeTaskCode = 0
	let section = document.createElement("section")
	section.id = "form"

	let form = document.createElement("form")
	section.appendChild(form)
	form.onsubmit = function(event) {
		event.preventDefault()

		let id = event.target.elements.id.value
		let title = event.target.elements.title.value
		let color = event.target.elements.color.value
		if (!title && !id) return close("form")

		api.modifyList(id, title, color, (response) => {
			let list_element = document.querySelector(`[code='${id}']`)
			list_element.style.backgroundColor = response.color
			list_element.innerHTML = response.title
		})
		close("form")

		return false
	}
	form.innerHTML = `
		<input type='hidden' name='id' value='${lista.id}'>
		<input type='text' name='title' placeholder='Nombre' value='${lista.title}'>
		<input type='text' name='color' placeholder='Color' value='${lista.color}'>
		<input type='submit' value='Modificar'>
		<button type='button' onclick="window.close('form')">Cancelar</button>
	`

	document.body.appendChild(section)
}

function openFormRemoveList(lista){
	
	activeTaskCode = 0
	let section = document.createElement("section")
	section.id = "form"

	let form = document.createElement("form")
	section.appendChild(form)

	form.onsubmit = function(event) {
		//alert("prueba")
		event.preventDefault()

		let id = event.target.elements.id.value

		api.removeList(id)
		document.querySelector(`a:not(.task)[code='${id}']`).parentElement.remove()
		close("form")
		let removeList = document.querySelector(`section[data-id='${id}']`)
		
		if (removeList) removeList.remove()

		return false
	}
	form.innerHTML = `
		<input type='hidden' name='id' value='${lista.id}'>
		<h2>¿Seguro que quieres eliminar la lista ${lista.title}?</h2>
		<input type='submit' value='Eliminar'>
		<button type='button' onclick="window.close('form')">Cancelar</button>
	`

	document.body.appendChild(section)
}


function openFormModifyTask(task) {

	activeTaskCode = 0
	let section = document.createElement("section")
	section.id = "form"

	let form = document.createElement("form")
	section.appendChild(form)
	form.onsubmit = function(event) {
		event.preventDefault()

		let id = event.target.elements.id.value
		let name = event.target.elements.name.value
		let description = event.target.elements.description.value
		let status = !event.target.elements.status.checked

		if (!name && !id) return close("form")

		let tasca = {
			"id": id,
			"name": name,
			"description": description,
			"status": status
		}
		api.modifyTask(tasca, (response) => {
			let task_element = document.querySelector(`a.task[code='${id}']`)
			task_element.innerHTML = `
				<h3>${response.name}</h3>
				<p>${response.description}</p>
			`
			if(status){
				task_element.classList.remove("disabled")
			} else{
				task_element.classList.add("disabled")
			}
		})
		close("form")

		return false
	}

	let estat = task.status ? "" : "checked"
	form.innerHTML = `
		<input type='hidden' name='id' value='${task.id}'>
		<input type='text' name='name' placeholder='Nombre' value='${task.name}'>
		<input type='text' name='description' placeholder='Descripción' value='${task.description}'>
		<br>
		<label><input type='checkbox' name='status' ${estat}> Tasca completada</label>
		<br>
		<input type='submit' value='Modificar'>
		<button type='button' onclick="window.close('form')">Cancelar</button>
	`

	document.body.appendChild(section)
}

function openFormNewTask(code) {
	close("form")
	let section = document.createElement("section")
	section.id = "form"

	let form = document.createElement("form")
	section.appendChild(form)
	form.onsubmit = function(event) {
		event.preventDefault()

		let title = event.target.elements.title.value
		let description = event.target.elements.description.value
		if (!title) return close("form")

		addTask(code, title, description)

		close("form")

		return false
	}

	form.innerHTML = `
		<input type='text' name='title' placeholder='Título'>
		<input type='text' name='description' placeholder='Descripción'>
		<input type='submit' value='Crear'>
		<button type='button' onclick="window.close('form')">Cancelar</button>
	`

	document.body.appendChild(section)
}


function addList(name, code, color) {
	let lista = document.getElementById('list')
	let li = document.createElement("li")
	let a = document.createElement("a")
	li.setAttribute("data-id", code)
	a.onclick = function() {
		openTasks(code)
	}
	a.oncontextmenu = function(event) {
		menu(event, "list")
	}
	a.innerHTML = name
	a.style.backgroundColor = color
	a.setAttribute("code", code)
	li.appendChild(a)
	lista.appendChild(li)
}

function addTask(listId, title, description) {

	let container = document.querySelector("#tasks > div")

	let a = document.createElement("a")
	a.classList.add("task")

	let task = api.addTask(title, description, (r) => {
		api.addTaskToList(listId, r, (res) => {
			let id = r.id
			a.setAttribute("code", id)
			a.setAttribute("data-id", id)
			a.onclick = function() {
				var t = api.getTaskById(id)
				t.status = !t.status
				
				//var t = {id : res.id, name : task.name, description: task.description, status: !task.status}
				api.modifyTask(
					t, 
					(r) => {
						if(t.status){
							this.classList.remove("disabled")
						} else{
							this.classList.add("disabled")
						}
					}
				)
			}
		})
	})
	a.oncontextmenu = function(event) {
		menu(event, "task")
	}

	a.innerHTML = `
		<h3>${task.name}</h3>
		<p>${task.description}</p>
	`
	

	container.appendChild(a)

}


function removeList(code) {
	close("menu")
 	var list = api.getListById(code)
	openFormRemoveList(list)
	//document.querySelector(`a:not(.task)[code='${code}']`).parentElement.remove()
}
function removeTask(code){
	close("form")
	api.removeTask(code)
	document.querySelector(`a.task[code='${code}']`).remove()
}

function modifyList(code) {
	close("menu")
	var list = api.getListById(code)
	openFormModifyList(list)
}

function modifyTask(code) {
	close("menu")
	var tasca = api.getTaskById(code)
	openFormModifyTask(tasca)
}
